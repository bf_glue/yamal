import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        events: [],
        profile: {},
    },
    getters: {
        profileShortName(state) {
            if (state.profile.last_name) {
                var name = state.profile.first_name[0] + '.'
                if (state.profile.third_name[0]) name += ' ' + state.profile.third_name[0] + '.'
                name += ' ' + state.profile.last_name
                return name
            }
            else {
                return state.profile.email
            }
        },
        profileFullName(state) {
            if (state.profile.last_name) {
                return `${state.profile.last_name} ${state.profile.first_name} ${state.profile.third_name}`
            }
            else {
                return state.profile.email
            }
        },
    },
    mutations: {
        SET_EVENTS (state, events) {
            state.events = events
        },
        SET_PROFILE (state, profile) {
            state.profile = profile
        },
    },
    actions: {
        fetchEvents({commit}) {
            return axios.get('/events/')
                .then((response) => commit('SET_EVENTS', response.data))
        },
        fetchProfile({commit}) {
            return axios.get('/profile/')
                .then((response) => commit('SET_PROFILE', response.data))
        },
    }
})