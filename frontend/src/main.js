// IMPORTS

import '@fortawesome/fontawesome-free/css/all.min.css';
import '@voerro/vue-tagsinput/dist/style.css';

import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router'
import store from './store'

import 'bootstrap'
import 'bootstrap/scss/bootstrap.scss';
// import 'babel-polyfill'

import BootstrapVue from 'bootstrap-vue'

import axios from 'axios'
import moment from 'moment'

// import './custom.css';

// CONFIGURATION

axios.defaults.baseURL = '/api';
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";

Vue.use(VueRouter);
Vue.use(BootstrapVue);


// ROUTING

import TestPage from './pages/TestPage.vue'
import EventsPage from './pages/EventsPage.vue'
import EventPage from './pages/EventPage.vue'
import EventConfirmPage from './pages/EventConfirmPage.vue'
import ProfilePage from './pages/ProfilePage.vue'
import RatingPage from './pages/RatingPage.vue'

const routes = [
    { name: 'test', path: '/test', component: TestPage },
    { name: 'events', path: '/', component: EventsPage, props: { mode: 'future' } },
    { name: 'events-all', path: '/all', component: EventsPage, props: { mode: 'filter', all: true } },
    { name: 'events-future', path: '/future', component: EventsPage, props: { mode: 'future' } },
    { name: 'events-past', path: '/past', component: EventsPage, props: { mode: 'past' } },
    { name: 'events-my-future', path: '/my', component: EventsPage, props: { mode: 'my-future' } },
    { name: 'events-my-past', path: '/my-past', component: EventsPage, props: { mode: 'my-past' } },
    { name: 'events-filters', path: '/filters', component: EventsPage, props: { mode: 'filter' } },
    { name: 'event', path: '/events/:id', component: EventPage, props: true },
    { name: 'event-confirm', path: '/events/:id/confirm', component: EventConfirmPage, props: true },
    { name: 'profile', path: '/profile', component: ProfilePage },
    { name: 'rating', path: '/rating', component: RatingPage },
]

const router = new VueRouter({
    routes: routes,
    mode: 'history',
    base: '/app'
})


// DIRECTIVES AND FILTERS

Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})

Vue.filter('formatDate', function(value, format) {
    if (!format) {
        format = 'DD-MM-YYYY hh:mm'
    }
    if (value) {
        return moment(String(value)).format(format)
    }
})


// STARTING

new Vue({
    router: router,
    store,
    render: h => h(App)
}).$mount('#app');
