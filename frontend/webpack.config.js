const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/static/',
    filename: 'build.js',
    // chunkFilename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'vue-style-loader',
          use: 'css-loader'
        }),
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'vue-style-loader',
          use: [
              'css-loader',
              'sass-loader'
          ]
        }),
        // use: [
        //   'vue-style-loader',
        //   'css-loader',
        //   'sass-loader'
        // ],
      },
      {
        test: /\.sass$/,
        use: ExtractTextPlugin.extract({
          fallback: 'vue-style-loader',
          use: [
              'css-loader',
              'sass-loader?indentedSyntax'
          ]
        }),
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          extractCSS: true,
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            'scss': [
              'vue-style-loader',
              'css-loader',
              'sass-loader'
            ],
            'sass': [
              'vue-style-loader',
              'css-loader',
              'sass-loader?indentedSyntax'
            ]
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'img/[name].[ext]' //?[hash]
        }
      },
      {
        test: /\.(otf|eot|woff|woff2|ttf)([\?]?.*)$/,
        loader: "file-loader",
        options: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#source-map',
  plugins: [
    new webpack.ProvidePlugin({
        '$': 'jquery',
        // '_': 'lodash'
    }),
    new ExtractTextPlugin("css/build.css"),
    new webpack.IgnorePlugin(/^codemirror$/),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
    // new webpack.optimize.CommonsChunkPlugin({
    //     name: 'common',
    //     children: true,
    //     minChunks: 2,
    //     async: true
    // })
  ],
  // externals: {
  //   lodash: 'lodash',
  //   jquery: 'jquery'
  // }
};


if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = false; // '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    // new webpack.optimize.UglifyJsPlugin({
    new UglifyJSPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
