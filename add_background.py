# import cv2
# import numpy as np

# overlay = cv2.imread('/work/projects/yamal/imgs/t_result.png')
# background = cv2.imread('/work/projects/yamal/imgs/back.png')

# added_image = cv2.addWeighted(background,0.1,overlay,1,0)

# cv2.imwrite('/work/projects/yamal/imgs/combined.png', added_image)

from PIL import Image

overlay = Image.open('/work/projects/yamal/static/imgs/t_result.png')
background = Image.open('/work/projects/yamal/static/imgs/back.png').convert('RGBA')



result = Image.alpha_composite(overlay, background)

result.save('/work/projects/yamal/static/imgs/combined.png')