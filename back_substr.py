from cv2 import *
import numpy as np

def remove_background(im):
	im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)
	# smooth the image with alternative closing and opening
	# with an enlarging kernel
	morph = im.copy()

	kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 1))
	morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)
	morph = cv2.morphologyEx(morph, cv2.MORPH_OPEN, kernel)

	kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))

	# take morphological gradient
	gradient_image = cv2.morphologyEx(morph, cv2.MORPH_GRADIENT, kernel)

	# split the gradient image into channels
	image_channels = np.split(np.asarray(gradient_image), 1, axis=2)

	channel_height, channel_width, _ = image_channels[0].shape

	# apply Otsu threshold to each channel
	for i in range(0, 3):
	    _, image_channels[i] = cv2.threshold(~image_channels[i], 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY)
	    image_channels[i] = np.reshape(image_channels[i], newshape=(channel_height, channel_width, 1))

	# merge the channels
	image_channels = np.concatenate((image_channels[0], image_channels[1], image_channels[2]), axis=2)

	# save the denoised image
	cv2.imwrite('/work/projects/yamal/media/result_mask_denoiced.jpg', image_channels)




back_1 = cv2.imread('/work/projects/yamal/media/back_1.jpg')
back_2 = cv2.imread('/work/projects/yamal/media/back_2.jpg')
back_3 = cv2.imread('/work/projects/yamal/media/back_3.jpg')
photo = cv2.imread('/work/projects/yamal/media/photo.jpg')

P1 = 200
P2 = 400
P3 = 5
P4 = 21

back_1 = cv2.fastNlMeansDenoisingColored(back_1,None,P1,P2,P3,P4)
back_2 = cv2.fastNlMeansDenoisingColored(back_2,None,P1,P2,P3,P4)
back_3 = cv2.fastNlMeansDenoisingColored(back_3,None,P1,P2,P3,P4)
photo = cv2.fastNlMeansDenoisingColored(photo,None,P1,P2,P3,P4)

backSub = cv2.createBackgroundSubtractorMOG2(500, 50, False)

fgMask = backSub.apply(back_1)
fgMask = backSub.apply(back_2)
fgMask = backSub.apply(back_3)
fgMask = backSub.apply(photo)


cv2.imwrite('/work/projects/yamal/media/result_mask.png', fgMask)
# remove_background(fgMask)


# cv2.imwrite('/work/projects/yamal/media/result_photo.png', photo)\