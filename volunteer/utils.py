# -*- coding: utf-8 -*-

import cv2
from cv2 import *
import time
from PIL import Image
import numpy as np

def get_photo_from_camera():
    img_path = "/work/projects/yamal/imgs/photo_from_camera.png"
    cam = VideoCapture(0)
    time.sleep(1)
    s, img = cam.read()
    if s:
        # imwrite(img_path,img)
        remove_background(img)
        add_background()
    return img_path


def add_background():
    overlay = Image.open('/work/projects/yamal/media/imgs/t_result.png')
    background_1 = Image.open('/work/projects/yamal/media/imgs/back_1.png').convert('RGBA')
    background_2 = Image.open('/work/projects/yamal/media/imgs/back_2.png').convert('RGBA')
    foreground_1 = Image.open('/work/projects/yamal/media/imgs/front_1.png')
    foreground_2 = Image.open('/work/projects/yamal/media/imgs/front_2.png')
    result_1 = Image.alpha_composite(background_1, overlay)
    result_2 = Image.alpha_composite(background_2, overlay)
    result_1 = Image.alpha_composite(result_1, foreground_1)
    result_2 = Image.alpha_composite(result_2, foreground_2)

    result_1.save('/work/projects/yamal/media/imgs/combined_1.png')
    result_2.save('/work/projects/yamal/media/imgs/combined_2.png')


def remove_background(img):
    #== Parameters =======================================================================
    BLUR = 89
    CANNY_THRESH_1 = 20
    CANNY_THRESH_2 = 40
    MASK_DILATE_ITER = 10
    MASK_ERODE_ITER = 15
    MASK_COLOR = (0.0,0.0,1.0) # In BGR format


    #== Processing =======================================================================

    #-- Read image -----------------------------------------------------------------------
    # img = cv2.imread('/work/projects/yamal/media/imgs/photo_from_camera.png')
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # background = cv2.imread('/work/projects/yamal/media/imgs/back_w.png')

    #-- Edge detection -------------------------------------------------------------------
    edges = cv2.Canny(gray, CANNY_THRESH_1, CANNY_THRESH_2)
    edges = cv2.dilate(edges, None)
    edges = cv2.erode(edges, None)

    #-- Find contours in edges, sort by area ---------------------------------------------
    contour_info = []
        
    # Previously, for a previous version of cv2, this line was: 
    contours, _ = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    # Thanks to notes from commenters, I've updated the code but left this note
    for c in contours:
        contour_info.append((
            c,
            cv2.isContourConvex(c),
            cv2.contourArea(c),
        ))
    contour_info = sorted(contour_info, key=lambda c: c[2], reverse=True)
    max_contour = contour_info[0]

    #-- Create empty mask, draw filled polygon on it corresponding to largest contour ----
    # Mask is black, polygon is white
    mask = np.zeros(edges.shape)
    cv2.fillConvexPoly(mask, max_contour[0], (255))

    #-- Smooth mask, then blur it --------------------------------------------------------
    mask = cv2.dilate(mask, None, iterations=MASK_DILATE_ITER)
    mask = cv2.erode(mask, None, iterations=MASK_ERODE_ITER)
    mask = cv2.GaussianBlur(mask, (BLUR, BLUR), 0)
    mask_stack = np.dstack([mask]*3)    # Create 3-channel alpha mask

    #-- Blend masked img into MASK_COLOR background --------------------------------------
    mask_stack  = mask_stack.astype('float32') / 255.0          # Use float matrices, 
    img         = img.astype('float32') / 255.0                 #  for easy blending

    c_red, c_green, c_blue = cv2.split(img)
    img_a = cv2.merge((c_red, c_green, c_blue, mask.astype('float32') / 255.0))
    cv2.imwrite('/work/projects/yamal/media/imgs/t_result.png', img_a*255)

    # masked = (mask_stack * img) + ((1-mask_stack) * MASK_COLOR) # Blend
    # masked = (masked * 255).astype('uint8')                     # Convert back to 8-bit

    # cv2.imwrite('/work/projects/yamal/imgs/result.png', masked)                                   # Display
    # cv2.waitKey()
    return img_a
    # return True



