from django.contrib import admin

from models import Manager, Event, UserProfile, Participations, Rank, Tag

admin.site.register(Manager)
admin.site.register(Event)
admin.site.register(UserProfile)
admin.site.register(Participations)
admin.site.register(Rank)
admin.site.register(Tag)