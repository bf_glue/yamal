from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from volunteer import views 
from django.conf.urls.static import static

urlpatterns = [
    url(r'^register/$', views.register),
    url(r'^register/success/$', views.register_success),
    url(r'^register/recover/', views.recover_password),
    url(r'^register/change-password/', views.change_password),

    url(r'^login/$', views.login),
    url(r'^logout/$', views.logout),

    url(r'^admin/', admin.site.urls),    

    url(r'^get_qr/', views.get_qr),
    url(r'^get_photo/', views.get_photo),
    url(r'^get_background/', views.get_background),

    url(r'^app/', views.app),
    url(r'^api/', include('volunteer.restapi.urls')),

    url(r'^$', views.index),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
