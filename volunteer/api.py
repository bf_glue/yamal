import json

from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.conf import settings

from models import *


def profile(request):

    if request.user.is_anonymous():
        return JsonResponse({ 'is_anonymous': True }, safe=False)

    result = {}
    user = request.user
    user_profile = UserProfile.objects.get(user=user)
    
    result['user_id'] = user.id
    result['email'] = user.email
    result['rank'] = user_profile.rank
    result['participation_count'] = user_profile.participation_count
    if user_profile.photo:
        result['photo'] = user_profile.photo.url
        result['thumb'] = user_profile.thumb
    else:
        result['photo'] = ''
        result['thumb'] = ''
    result['first_name'] = user_profile.first_name
    result['last_name'] = user_profile.last_name
    result['third_name'] = user_profile.third_name
    result['place'] = user_profile.place

    result['full_name'] = ' '.join([result['last_name'], result['first_name'], result['third_name']]).strip()

    result['about'] = user_profile.about
    result['points'] = user_profile.points
    
    if user.is_superuser:
        result['is_admin'] = True
    else:
        result['is_admin'] = False

    if user.is_staff:
        result['is_staff'] = True
    else:
        result['is_staff'] = False
        
    return JsonResponse(result, safe=False)


@login_required
def profile_save(request):
    r = json.loads(request.body)
    user = request.user
    user_profile = UserProfile.objects.get(user=user)

    user_profile.first_name = r['first_name']
    user_profile.last_name = r['last_name']
    user_profile.third_name = r['third_name']
    user_profile.about = r['about']
    # user_profile.points = r['points']
    
    user_profile.save()

    return HttpResponse()

@login_required
def join_to_event(request, event_id):
    user = request.user
    event = Event.objects.get(pk=event_id)
    
    participation, created = Participations.objects.get_or_create(
        user=user,
        event=event
    )

    if created:
        participation.state = 1
        participation.save()
        return HttpResponse('OK')
    else:
        return HttpResponse('Participation already exist')

    
@login_required
def exit_from_event(request, event_id):
    user = request.user
    event = Event.objects.get(pk=event_id)

    try:
        participation = Participations.objects.get(user=user, event=event)
        participation.delete()
        return HttpResponse('OK')
    except Participations.DoesNotExist:
        return HttpResponse('Participation does not exist')


@login_required
def not_participated(request, event_id):
    user = request.user
    event = Event.objects.get(pk=event_id)

    try:
        participation = Participations.objects.get(user=user, event=event)
        participation.state = 0
        participation.save()
        return HttpResponse('OK')
    except Participations.DoesNotExist:
        return HttpResponse('Participation does not exist')


@login_required
def upload_participation_photo(request, event_id):
    user = request.user
    event = Event.objects.get(pk=event_id)

    if request.method == 'POST':
        try:
            file = request.FILES.getlist('files')[0]
        except:
            return HttpResponse('No file provided')
        
        try:
            participation = Participations.objects.get(user=user, event=event)
            participation.photo = file
            participation.state = 2
            participation.save()
            return HttpResponse('OK')
        except Participations.DoesNotExist:
            return HttpResponse('Participation does not exist')
    else:
        return HttpResponse()


@login_required
def check_participation(request, event_id):
    result = {}
    user = request.user
    event = Event.objects.get(pk=event_id)
    
    try:
        participation = Participations.objects.get(user=user, event=event)
        result['state'] = participation.state
        if participation.state >= 2:
            result['photo'] = participation.thumb_500
        return JsonResponse(result, safe=False)
    except Participations.DoesNotExist:
        result['state'] = None
        return JsonResponse(result, safe=False)


@login_required
def upload_event_image(request, event_id):
    if request.method == 'POST':
        try:
            file = request.FILES.getlist('files')[0]
        except:
            return HttpResponse('No file provided')

        event = Event.objects.get(pk=event_id)
        event.image = file
        event.save()
        return HttpResponse('OK')
    else:
        return HttpResponse()


@login_required
def my_past_events(request):
    result = {}
    
    waiting_events = Event.objects.filter(
        pk__in=request.user.participations.filter(state=1).values_list('event_id', flat=True)
    )
    if waiting_events.count() > 0:
        result['waiting'] = []
        for event in waiting_events:
            result['waiting'].append({
                'id': event.id,
                'header': event.header,
                'short_description': event.short_description,
                'thumb': event.thumb
            })

    moderation_events = Event.objects.filter(
        pk__in=request.user.participations.filter(state=2).values_list('event_id', flat=True)
    )
    if moderation_events.count() > 0:
        result['moderation'] = []
        for event in moderation_events:
            result['moderation'].append({
                'id': event.id,
                'header': event.header,
                'short_description': event.short_description,
                'thumb': event.thumb
            })

    confirmed_events = Event.objects.filter(
        pk__in=request.user.participations.filter(state=3).values_list('event_id', flat=True)
    )
    if confirmed_events.count() > 0:
        result['confirmed'] = []
        for event in confirmed_events:
            result['confirmed'].append({
                'id': event.id,
                'header': event.header,
                'short_description': event.short_description,
                'thumb': event.thumb
            })

    declined_events = Event.objects.filter(
        pk__in=request.user.participations.filter(state=4).values_list('event_id', flat=True)
    )
    if declined_events.count() > 0:
        result['declined'] = []
        for event in declined_events:
            result['declined'].append({
                'id': event.id,
                'header': event.header,
                'short_description': event.short_description,
                'thumb': event.thumb
            })

    return JsonResponse(result, safe=False)


@login_required
def new_tags(request, event_id):
    r = json.loads(request.body)
    event = Event.objects.get(pk=event_id)
    if type(r) == list:
        for tag in r:
            tag = Tag.objects.create(name=tag)
            event.tags.add(tag)
        return HttpResponse('ok')
    return HttpResponse('No tags provided')


@login_required
def upload_profile_photo(request):
    if request.method == 'POST':
        try:
            file = request.FILES.getlist('files')[0]
        except:
            return HttpResponse('No file provided')
        user_profile = UserProfile.objects.get(user=request.user)
        user_profile.photo = file
        user_profile.save()
        return HttpResponse('OK')
    else:
        return HttpResponse()


@login_required
def delete_profile_photo(request):   
    user_profile = UserProfile.objects.get(user=request.user)
    user_profile.photo = None
    user_profile.save()
    return HttpResponse('OK')


# @staff_member_required
@login_required
def grupped_participations_to_aprove(request):
    result = []
    participations = Participations.objects.filter(state=2)
    event_ids = participations.values_list('event',flat=True).distinct()
    events = Event.objects.filter(pk__in=event_ids)

    for event in events:
        tmp_result = {}
        tmp_result['event_id'] = event.id
        tmp_result['event_header'] = event.header
        tmp_result['participations'] = []

        for participation in participations.filter(event=event):
            tmp_result['participations'].append({
                'id': participation.id,
                'fio': participation.user.userprofile.fio,
                'photo': participation.photo.url,
                'thumb_120': participation.thumb_120
            })
        
        result.append(tmp_result)
    
    return JsonResponse(result, safe=False)



# @staff_member_required
@login_required
def approve_participation(request, participation_id):
    try:
        participation = Participations.objects.get(pk=participation_id)
        participation.state = 3
        participation.save()
        
        participation.user.userprofile.points += 1
        participation.user.userprofile.save()

        next_participations = Participations.objects.filter(event=participation.event, state=2)
        if next_participations.count() > 0:
            return JsonResponse({'next': next_participations[0].id}, safe=False)
        else:
            return JsonResponse({'next': None}, safe=False)

    except Participations.DoesNotExist:
        return HttpResponse()

# @staff_member_required
@login_required
def decline_participation(request, participation_id):
    try:
        participation = Participations.objects.get(pk=participation_id)
        participation.state = 4
        participation.save()
        next_participations = Participations.objects.filter(event=participation.event, state=2)
        if next_participations.count() > 0:
            return JsonResponse({'next': next_participations[0].id}, safe=False)
        else:
            return JsonResponse({'next': None}, safe=False)

    except Participations.DoesNotExist:
        return HttpResponse()


@login_required
def participation_card(request, participation_id):
    try:
        participation = Participations.objects.get(pk=participation_id)
    except Participations.DoesNotExist:
        return HttpResponse('Participations does not exist')
    
    result = {
        'event_header': participation.event.header,
        'fio': participation.user.userprofile.fio,
        'thumb_500': participation.thumb_500,
        'confirmation_requirements': participation.event.confirmation_requirements
    }
    return JsonResponse(result, safe=False)