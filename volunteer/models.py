# -*- coding: utf-8 -*-

import settings
from django.db import models
from sorl.thumbnail import get_thumbnail
from django.contrib.auth.models import User
from django.dispatch import receiver


class Manager(models.Model):
    name = models.CharField(max_length=250, blank=True)
    image = models.ImageField(upload_to='uploads/')
    description = models.TextField(blank=True)

    @property
    def thumb(self):
        try:
            thumb = get_thumbnail(self.image, '500x500', upscale=False, crop='center', quality=85)
            return thumb.url
        except:
            return None

    def __unicode__(self):
        return self.name


class Event(models.Model):
    header = models.CharField(max_length=250, blank=True)
    image = models.ImageField(upload_to='uploads/', blank=True)
    date = models.DateField(null=True)
    place = models.CharField(max_length=250, blank=True)
    short_description = models.TextField(blank=True)
    text = models.TextField(blank=True)
    confirmation_requirements = models.TextField(blank=True)
    publish = models.BooleanField(default=True)
    tags = models.ManyToManyField("Tag", blank=True)
    manager = models.ForeignKey(Manager, blank=True, null=True)

    @property
    def thumb(self):
        try:
            thumb = get_thumbnail(self.image, '500x500', upscale=False, crop='center', quality=85)
            return thumb.url
        except:
            return None

    def __unicode__(self):
        return self.header


class Tag(models.Model):
    name = models.CharField(max_length=250, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    photo = models.ImageField(upload_to='uploads/', blank=True)
    first_name = models.CharField(max_length=250, blank=True)
    last_name = models.CharField(max_length=250, blank=True)
    third_name = models.CharField(max_length=250, blank=True)
    about = models.TextField(blank=True)
    points = models.PositiveIntegerField(default=0)
    recovery_key = models.CharField(max_length=250, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)

    @property
    def rank(self):
        ranks = Rank.objects.filter(min_points__lte=self.points)
        if ranks.count() > 0:
            return ranks.latest('min_points').name
        else:
            return 'Без статуса'

    @property
    def fio(self):
        return self.last_name + " " + self.first_name[0] + "." + self.third_name[0] + "."

    @property
    def participation_count(self):
        return Participations.objects.filter(user=self.user, state=3).count()

    @property
    def place(self):
        return UserProfile.objects.filter(points__gte=self.points).order_by('-points').values_list('points', flat=True).distinct().count()
    
    @property
    def thumb(self):
        try:
            thumb = get_thumbnail(self.photo, '500x500', upscale=False, quality=85)
            return thumb.url
        except:
            return None

    def __unicode__(self):
        return self.user.email or self.fio


class Participations(models.Model):
    STATE_CHOICES = (
        (0, 'Не принял участия'),
        (1, 'Зарегистрирован'),
        (2, 'Загружено подтверждение'),
        (3, 'Подтверждение принято'),
        (4, 'Подтверждение отвергнуто')
    )
    event = models.ForeignKey(Event)
    user = models.ForeignKey(User, related_name="participations")
    photo = models.ImageField(upload_to='uploads/', blank=True)
    state = models.PositiveIntegerField(choices=STATE_CHOICES, default=1)

    @property
    def thumb_120(self):
        try:
            thumb = get_thumbnail(self.photo, '120x120', upscale=False, quality=85)
            return thumb.url
        except:
            return None

    @property
    def thumb_500(self):
        try:
            thumb = get_thumbnail(self.photo, '500x500', upscale=False, quality=85)
            return thumb.url
        except:
            return None

    @property
    def thumb_1200(self):
        try:
            thumb = get_thumbnail(self.photo, '1200x1200', upscale=False, quality=85)
            return thumb.url
        except:
            return None    

    def __unicode__(self):
        return self.user.email + ' - ' + self.event.header


class Rank(models.Model):
    min_points = models.PositiveIntegerField()
    name = models.CharField(max_length=250, blank=True)

    def __unicode__(self):
        return self.name

