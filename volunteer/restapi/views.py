# -*- coding: utf-8 -*-
import datetime

from django_filters.rest_framework import DjangoFilterBackend

from django.contrib.auth.models import User, Group
from django.db.models import Q

from rest_framework import viewsets, generics, pagination
from serializers import *

from volunteer.models import *


class StandardResultsSetPagination(pagination.PageNumberPagination):
    page_size = 30
    page_size_query_param = 'page_size'
    # max_page_size = 100


class ManagerViewSet(viewsets.ModelViewSet):
    queryset = Manager.objects.all()
    serializer_class = ManagerSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    def get_queryset(self):
        queryset = Event.objects.all()

        mode = self.request.query_params.get('mode', None)
        date_from = self.request.query_params.get('date_from', None)
        date_to = self.request.query_params.get('date_to', None)
        tags = self.request.query_params.get('tags', None)
        # all_events = self.request.query_params.get('all', None)

        # if not all_events:
        #     queryset = queryset.filter(publish=True)

        if mode:
            if mode == 'future':
                queryset = queryset.filter(date__gte=datetime.datetime.now())
            if mode == 'past':
                queryset = queryset.filter(date__lt=datetime.datetime.now())
            if mode == 'my-future':
                event_ids = self.request.user.participations.filter(event__date__gte=datetime.datetime.now()).values_list('event_id', flat=True)
                queryset = queryset.filter(id__in=event_ids)
            if mode == 'my-past':
                event_ids = self.request.user.participations.filter(event__date__lt=datetime.datetime.now()).values_list('event_id', flat=True)
                queryset = queryset.filter(id__in=event_ids)
            if mode == 'filter':
                if date_from:
                    dd = date_from.split('-')
                    datetime_from = datetime.datetime(int(dd[2]),int(dd[1]),int(dd[0]))
                    queryset = queryset.filter(date__gte=datetime_from)
                if date_to:
                    dd = date_to.split('-')
                    datetime_to = datetime.datetime(int(dd[2]),int(dd[1]),int(dd[0]))
                    queryset = queryset.filter(date__lte=datetime_to).distinct()
                if tags:
                    tag_ids = tags.split(',')
                    queryset = queryset.filter(tags__in=tag_ids).distinct()

        return queryset


class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer


class ParticipationsViewSet(viewsets.ModelViewSet):
    queryset = Participations.objects.all()
    serializer_class = ParticipationsSerializer


class RankViewSet(viewsets.ModelViewSet):
    queryset = Rank.objects.all()
    serializer_class = RankSerializer


class RatingViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all().order_by("-points")
    serializer_class = UserProfileSerializer
    pagination_class = StandardResultsSetPagination


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
