# -*- coding: utf-8 -*-

from django.conf.urls import include, url

from rest_framework import routers
from volunteer.restapi import views
from volunteer.api import *


restapi_router = routers.DefaultRouter()
restapi_router.register(r'managers', views.ManagerViewSet, base_name='managers')
restapi_router.register(r'events', views.EventViewSet, base_name='events')
# restapi_router.register(r'profiles', views.UserProfileViewSet, base_name='profiles')
restapi_router.register(r'participations', views.ParticipationsViewSet, base_name='participations')
restapi_router.register(r'ranks', views.RankViewSet, base_name='ranks')
restapi_router.register(r'rating', views.RatingViewSet, base_name='rating')
restapi_router.register(r'tags', views.TagViewSet, base_name='tags')


urlpatterns = [
    
    url(r'^', include(restapi_router.urls)),
    url(r'^profile/$', profile),
    url(r'^profile/upload_photo/$', upload_profile_photo),
    url(r'^profile/delete_photo/$', delete_profile_photo),
    url(r'^profile_save/$', profile_save),

    url(r'^events/(\d+)/join/$', join_to_event),
    url(r'^events/(\d+)/exit/$', exit_from_event),
    url(r'^events/(\d+)/not_participated/$', not_participated),
    url(r'^events/(\d+)/upload/$', upload_participation_photo),
    url(r'^events/(\d+)/check/$', check_participation),
    url(r'^events/(\d+)/upload_image/$', upload_event_image),
    url(r'^events/(\d+)/new-tags/', new_tags),
    url(r'^my-past-events/$', my_past_events),

    url(r'^c-participations/$', grupped_participations_to_aprove),
    url(r'^c-participations/(\d+)/$', participation_card),
    url(r'^c-participations/(\d+)/approve/$', approve_participation),
    url(r'^c-participations/(\d+)/decline/$', decline_participation),
]