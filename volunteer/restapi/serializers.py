# -*- coding: utf-8 -*-

import datetime
from rest_framework import serializers
from django.contrib.auth.models import User

from volunteer.models import *


class ManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manager
        fields = ('id', 'name', 'thumb', 'description', 'image')
        

class EventSerializer(serializers.ModelSerializer):
    manager = ManagerSerializer(many=False, read_only=True)
    class Meta:
        model = Event
        fields = ('id', 'header', 'date', 'image', 'thumb', 'confirmation_requirements', 'place', 'publish', 'short_description', 'tags', 'text', 'manager')

    def to_representation(self, instance):
        r = super(EventSerializer, self).to_representation(instance)
        r['is_actual'] = instance.date > datetime.date.today()
        return r
        

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class UserProfileSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    class Meta:
        model = UserProfile
        fields = '__all__'


class ParticipationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participations
        fields = ('id', 'photo', 'state', 'event', 'user', 'thumb_500', 'thumb_120', 'thumb_1200')


class RankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rank
        fields = '__all__'

