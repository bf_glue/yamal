# -*- coding: utf-8 -*-

import time
import socket
import qrcode

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from cv2 import *
from volunteer.utils import get_photo_from_camera

from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.conf import settings
import hashlib, random

from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as django_logout, authenticate, login as django_login

from volunteer.models import UserProfile


def get_qr(request):
    hostname = socket.gethostname()    
    IPAddr = socket.gethostbyname(hostname)
    url = 'http://' + IPAddr + ':8000/get_photo/'
    img = qrcode.make(url)
    response = HttpResponse(content_type="image/png")
    img.save(response, 'png')
    return response


def get_photo(request):
    img_path = get_photo_from_camera()
    print img_path
    return render(request, "get_photo.html")


def get_background(request):
    cam = VideoCapture(0)
    time.sleep(1)
    s, img = cam.read()
    if s:
        imwrite("/work/projects/yamal/imgs/background_from_camera.png",img)
    return render(request, "get_photo.html")


def app(request):
    return render(request, "app.html")


def index(request):
    return HttpResponseRedirect('/app/')


def register(request):
    if request.method == 'POST':

        email = request.POST.get('username')
        email = email.replace(' ', '')

        if not ('@' in email and '.' in email):
            return HttpResponseRedirect('/register/?error=email')

        user = User.objects.filter(username=email)

        if user:
            # Уже зарегистрирован
            return HttpResponseRedirect('/register/?error=exists&email=' + email)
        else:
            password = request.POST.get('password', '')
            new_user = User.objects.create_user(email, email, password)
            new_profile = UserProfile.objects.create(
                user=new_user,
                first_name=request.POST.get('first_name', ''),
                last_name=request.POST.get('last_name', ''),
                third_name=request.POST.get('third_name', ''),
                about=request.POST.get('about', '')
            )
            # send_quick_registration_email(new_user, password)
            auth = authenticate(username=email, password=password)
            django_login(request, auth)

            return HttpResponseRedirect('/register/success/?email=' + email)

    context = {
        'error': request.GET.get('error', ''),
        'email': request.GET.get('email', ''),
    }
    return render(request, 'registration/register.html', context)


def register_success(request):
    context = {'email': request.GET.get('email', '')}
    return render(request, 'registration/success.html', context)


def recover_password(request):
    recover_email = request.GET.get('recover-email')
    try:
        user_profile = UserProfile.objects.get(user__email=recover_email)
        m = hashlib.md5()
        m.update(str(random.randrange(0, 9999999)) + settings.SECRET_KEY)
        user_profile.recovery_key = m.hexdigest()
        user_profile.save()

        send_mail(
            'Восстановление пароля',
            u'''Здравствуйте.

Вы получили это письмо, потому что поступил запрос на восстановление пароля.
Чтобы получить новый пароль, пройдите по этой ссылке:

http://%s/login?password-recovery-key=%s/

Внимание: если Вы не знаете, кто запрашивал восстановление пароля, то, пожалуйста, свяжитесь с нами.''' % (settings.SITE_DOMAIN ,user_profile.recovery_key),
            'posttry@yandex.ru.ru',
            [recover_email],
            fail_silently=False,
        )
        return HttpResponseRedirect('/login/?password_recover_success=true')
    except UserProfile.DoesNotExist:
        return HttpResponseRedirect('/login/?password_recover_success=false')


def change_password(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        key = request.POST.get('key')
        password = request.POST.get('password')
        if email and key and password:
            try:
                user_profile = UserProfile.objects.get(
                    user__email=email,
                    recovery_key=key
                )
                user = user_profile.user
                user.change_password(password)
                user.save()
                
                return HttpResponseRedirect('/login/?password_changed=true')

            except UserProfile.DoesNotExist:
                return HttpResponse("Password din't change", status=403)
        else:
            return HttpResponse("Password din't change", status=403)


def login(request):
    if request.method == 'POST':
        if request.POST.get('username'):
            username = request.POST['username']
            password = request.POST['password']
            username = username.replace(' ', '')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                django_login(request, user)
                next = request.GET.get('next', settings.LOGIN_REDIRECT_URL)
                response = HttpResponseRedirect(next)
                return response
            else:
                user_exists = User.objects.filter(username=username, is_active=False)
                if user_exists:
                    return HttpResponseRedirect('/login/?not_active=1&username=' + username)
                else:
                    return HttpResponseRedirect('/login/?invalid=1&username=' + username)
        else:
            return HttpResponse("No username provided", status=403)
    else:
        context = { 'email': request.GET.get('email', ''), 'invalid': request.GET.get('invalid', '') }
        return render(request, 'registration/login.html', context)


@login_required
def logout(request):
    django_logout(request)
    return HttpResponseRedirect('/login/')
